<!DOCTYPE html>
<?php
	@session_destroy()
?>
<html>
	<head>
		<link rel="stylesheet" href="style.css" type="text/css" media="screen">
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>OpenShare Login</title>
	</head>
	<body>
	     <div id="container">
		<h1>OpenShare</h1>
		<p id="tagline">Welcome to free file sharing!</p>
		<form action="portal.php" method="POST">
			<label>
				Username:
				<input type="text" name="username" />
			</label>
			<input type="submit" value="Log In" />
		</form>
		<p id="notSigned">Not signed up yet? Register by entering a new username below:</p>
		<form action="register.php" method="POST">
			<label>
				Desired Username:
				<input type="text" name="usernameAttempt" />
			</label>
			<input type="submit" value="Register" />
		</form>
	     </div>
	</body>
</html>

