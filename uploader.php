<?php
    session_start();
        $filename = basename($_FILES['uploadedfile']['name']);
            if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
		    echo "No file selected, or invalid filename. Go back and try again.";
		    exit;
		}
	    $user = $_SESSION['user'];
	    if( !preg_match('/^[\w_\-]+$/', $user) ){
		    echo "Invalid username";
		    exit;
	    }
	    $full_path = sprintf("uploads/%s/%s", $user,$filename);

        if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path)){
	    header("Location: home.php");
	    exit;
	}
        else{
                echo "Upload failed. We are sorry for our error. \n";
        }
        
    ?>
<html><br><br>
    <a href="home.php"> Return Home.</a>
</html>
        