<!DOCTYPE html>
	<?php
		session_start();
	?>
<html>
	<head>
		<link rel="stylesheet" href="style.css" type="text/css" media="screen">
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>OpenShare</title>
	</head>
	<body>
	<div id='container2'>
		<?php
			$user = $_SESSION['user'];
			echo "Welcome, ". $user ."!";
		?>
		
		<form class="logout">
			<a href="signin.php">Logout</a> 
		</form>
		
		
		<form class="uploader" enctype="multipart/form-data" action="uploader.php" method="POST">
			
				<input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
				<label id="UploadText" for="file_input">
					Upload:
				</label>
				<input name="uploadedfile" type="file" id="file_input" />
				<input id="uploadButton" type="submit" value="Upload File" />
			
		</form>
		
		
			
			
	<div id="container3">
		<h2> Your files</h2>
		<ul>
		<?php
			//Create variable for the user's directory of files
			$userDirectory = "uploads/".$user;
			
			//Check if the directory is empty
			if(is_dir_empty($userDirectory)){
				echo "<li> You have no uploads. Build a collection of files by starting uploading now!</li>";
			}
			else{
			//Directory is not empty. Iterate through the userDirectory and display each files with its own options: view, download, and delete.
				$iterator = new DirectoryIterator($userDirectory);
					foreach ($iterator as $fileinfo) {
						if (!$fileinfo->isDot()) {
							$curFile = $fileinfo->getFilename();
							$filePath = $userDirectory."/".$curFile;
							//<a href='$filePath'>View</a>
							echo  "<li> <p class='fileTitle'>$curFile</p> <form class='fileButton' action='$filePath' method='get'><button  name='view' type='submit'>View</button></form><form class='fileButton' action='deleteFile.php' method='POST'><button name='delete' type='submit' value='$filePath'>Delete</button></form></li>";
							//echo  "<li><form><input
						}
					}
			}
				

			
			
			//Function to check if a directory is empty.
			function is_dir_empty($dir) {
				if(!is_readable($dir)) return NULL;
				$handle = opendir($dir);
				while (false !== ($entry = readdir($handle))){
					if($entry != "." && $entry != "..") {
						return false;
					}
				}
			return true;
			}
		?>
		
		</ul>
		
	
	</div>
	</div>
	</body>
</html>
