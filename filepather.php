<?php

	function filepath ($filename){
		if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
			//echo "Invalid filename";
			exit;
		}
		$user = $_SESSION['user'];
		if( !preg_match('/^[\w_\-]+$/', $user) ){
			//echo "Invalid username";
			exit;
		}
		$full_path = sprintf("uploads/%s/%s", $user,$filename);
		return $full_path;
	}
?>
